<?php

namespace App\Console\Commands;

use App\Services\Jogo\JogoGourmetService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class JogoGourmet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:jogo-gourmet {resetar?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jogo Gourmet';

    /**
     * Execute the console command.
     */
    public function handle(JogoGourmetService $jogoGourmetService): void
    {
        $this->inicializarDadosBasicos($jogoGourmetService);
        $jogoGourmetService->executar(true);
    }

    private function inicializarDadosBasicos(JogoGourmetService $jogoGourmetService) : void
    {

       $this->argument('resetar') === 'resetar' && Redis::flushDB();

        if(!count($jogoGourmetService->obterPerguntasRepository()->listar())){
            $perguntaBasica = [
                'massa' => [
                    "lasanha" => "lasanha",
                ]
            ];

            $jogoGourmetService->obterPerguntasRepository()->criar($perguntaBasica);
        }
    }
}
