<?php

namespace App\Repositories\JogoPergunta;

use App\Repositories\RedisRepository;

class JogoPerguntaGourmetRepository extends RedisRepository implements iJogoPerguntaRepository
{

    public function criar(array $dados): void
    {
        $this->inserir($dados);
    }

    public function obterNomeDaTabela(): string
    {
        return 'pratos';
    }
}
