<?php

namespace App\Repositories\JogoPergunta;

interface iJogoPerguntaRepository
{
    public function criar(array $dados) : void;

    public function listar() : array;
}
