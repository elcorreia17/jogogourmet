<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Redis;

abstract class RedisRepository
{
    public abstract function obterNomeDaTabela(): string;

    public function listar(): array
    {
        $registros = Redis::lrange($this->obterNomeDaTabela(), 0, -1) ?? [];

        return $this->transformarEmArray($registros);
    }

    protected function inserir(array $registro): void
    {
        Redis::rpush(
            $this->obterNomeDaTabela(),
            json_encode($registro)
        );
    }

    private function transformarEmArray(array $registros): array
    {
        return array_map(function ($registro) {
            return json_decode($registro, true);
        }, $registros);
    }
}
