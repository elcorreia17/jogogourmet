<?php

namespace App\Services\Command;

use App\Helpers\LeituraEntrada;
use App\Helpers\Texto;
class CommandService implements iCommandService
{
    public function __construct(private readonly LeituraEntrada $entradaHelper)
    {}

    public function escreverLinha(string $textoPrompt, bool $obrigarResposta = false): ?string
    {
        $resposta = trim($this->entradaHelper->ler($textoPrompt));

        if($obrigarResposta === true && !$resposta){
            $this->escreverLinha($textoPrompt, $obrigarResposta);
        }

        return $resposta;
    }

    public function escreverLinhaComRespostaFixa(string $textoPrompt, array $opcoesResposta) : string
    {

        $opcoes = $this->tratarArrayDeOpcoes($opcoesResposta);

        $resposta = $this->tratarTexto($this->entradaHelper->ler($textoPrompt));

        if(!in_array($resposta, $opcoes)){
            $this->escreverLinhaComRespostaFixa($textoPrompt, $opcoes);
        }

        return $resposta;
    }

    private function tratarTexto(?string $resposta) : ?string
    {
        $resposta = Texto::removerAcentos($resposta);
        $resposta = Texto::transformarEmMinusculo($resposta);
        $resposta = Texto::removerTodosOsEspacos($resposta);

        return $resposta;
    }

    private function tratarArrayDeOpcoes(array $opcoes) : array
    {
        return array_map(function ($opcao) {

            return $this->tratarTexto($opcao);

        }, $opcoes);
    }
}
