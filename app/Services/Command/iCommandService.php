<?php

namespace App\Services\Command;

interface iCommandService
{
    public function escreverLinha(string $textoPrompt, bool $obrigarResposta = false) : ?string;
    public function escreverLinhaComRespostaFixa(string $textoPrompt, array $opcoesResposta) : string;
}
