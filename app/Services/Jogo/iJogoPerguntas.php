<?php

namespace App\Services\Jogo;

use App\Repositories\JogoPergunta\iJogoPerguntaRepository;

interface iJogoPerguntas
{
    public function executar(bool $resetar = false) : void;

    public function obterPerguntasRepository() : iJogoPerguntaRepository;

    public function resetar() : void;

    public function obterOpcoesDeConfirmacao() : array;

    public function obterNovaPergunta(string $ultimaPergunta) : array;

    public function obterTodasPerguntas() : array;

    public function obterPerguntaASerRealizada(string $mixed) : string;

    public function obterMensagemDeRespostaEncontradaComSucesso() : string;
}
