<?php

namespace App\Services\Jogo;

use App\Helpers\Texto;
use App\Repositories\JogoPergunta\iJogoPerguntaRepository;
use App\Services\Command\iCommandService;

abstract class JogoPerguntas implements iJogoPerguntas
{
    private array $perguntas = [];
    private string $perguntaAtual = "";

    public function __construct(
        protected readonly iJogoPerguntaRepository $perguntaRepository,
        protected readonly iCommandService         $commandService
    )
    {}
    public function executar(bool $resetar = false): void
    {
        $resetar && $this->resetar();

        $this->perguntas = $this->obterTodasPerguntas();

        $this->percorrerPerguntas($this->perguntas);

        $this->perguntaRepository->criar($this->perguntas);

        $this->executar(true);
    }
    private function percorrerPerguntas(array &$perguntas = []){
        $this->perguntaAtual = "";

        foreach ($perguntas as $categoria => &$nomeDoPrato){

            $resposta = $this->realizarPergunta($categoria);

            if ($this->perguntaFoiConfirmada($resposta)) {

                if(is_array($nomeDoPrato)){
                    return $this->percorrerPerguntas($nomeDoPrato);
                }

                if ($this->perguntaFoiConfirmada($resposta)) {
                    $this->commandService->escreverLinha(
                        $this->obterMensagemDeRespostaEncontradaComSucesso()
                    );

                    $this->executar(true);
                }

                $this->perguntaAtual = $nomeDoPrato;
            }

            is_string($nomeDoPrato) && $this->perguntaAtual = $nomeDoPrato;
        }

        $novaPergunta =  $this->obterNovaPergunta($this->obtemPerguntaAtual($perguntas));

        $perguntas[$novaPergunta['tipo']] = [$novaPergunta['nome'] => $novaPergunta['nome'] ];
    }

    private function obtemPerguntaAtual(array $perguntas) : string
    {
        $perguntaAtual = $this->perguntaAtual;

        if($perguntaAtual){
            return $perguntaAtual;
        }

        if($perguntas && $primeiraPergunta = key(collect($perguntas)->first()) ){
            return $primeiraPergunta;
        }

        return "";
    }

    private function realizarPergunta(string $dadosPergunta): string
    {
        $pergunta = $this->obterPerguntaASerRealizada($dadosPergunta);

        return $this->commandService->escreverLinhaComRespostaFixa($pergunta, $this->obterOpcoesDeConfirmacao());
    }

    public function obterOpcoesDeConfirmacao(): array
    {
        return ['Sim', 'Não'];
    }

    private function perguntaFoiConfirmada(string $textoResposta): bool
    {
        return Texto::transformarEmMinusculo($textoResposta) === 'sim';
    }

    public function obterPerguntasRepository(): iJogoPerguntaRepository
    {
        return $this->perguntaRepository;
    }
}
