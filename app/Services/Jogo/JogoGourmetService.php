<?php

namespace App\Services\Jogo;

class JogoGourmetService extends JogoPerguntas
{
    public function resetar() : void
    {
        $this->commandService->escreverLinha("Pense em um prato que gosta (pressione enter após pensar)! ");
    }
    public function obterTodasPerguntas() : array
    {
        return last($this->perguntaRepository->listar()) ?: [];
    }
    public function  obterPerguntaASerRealizada(string $dados) : string
    {
        $opcoes = $this->obterOpcoesDeConfirmacao();
        return "O prato que você pensou é {$dados} (" . implode(" | ", $opcoes) . "): ";
    }

    public function obterNovaPergunta(string $ultimaPergunta) : array
    {

        $novoPrato = $this->commandService->escreverLinha("Qual o prato você pensou: ", true);

        $textoNoTipo = "{$novoPrato} é ________ ";

        $ultimaPergunta && $textoNoTipo.= " mas {$ultimaPergunta} não?";

        $novoTipoPrato = $this->commandService->escreverLinha($textoNoTipo, true);

        return [
            'nome' => $novoPrato,
            'tipo' => $novoTipoPrato
        ];
    }

    public function obterMensagemDeRespostaEncontradaComSucesso() : string
    {
        return "Acertei de novo!";
    }
}
