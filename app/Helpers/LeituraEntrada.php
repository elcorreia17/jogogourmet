<?php

namespace App\Helpers;

class LeituraEntrada
{
    public function ler(string $textoPrompt) : ?string
    {
        return readline($textoPrompt);
    }
}
