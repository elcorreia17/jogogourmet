<?php

namespace App\Helpers;

class Texto
{
    public static function removerAcentos(string $texto) : string
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $texto);
    }

    public static function transformarEmMinusculo(string $texto) : string
    {
        return mb_strtolower($texto);
    }

    public static function removerTodosOsEspacos(string $texto) : string
    {
        return trim(preg_replace('/\s+/', '', $texto));
    }
}
