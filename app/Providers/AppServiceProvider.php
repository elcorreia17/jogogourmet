<?php

namespace App\Providers;

use App\Helpers\LeituraEntrada;
use App\Repositories\JogoPergunta\iJogoPerguntaRepository;
use App\Repositories\JogoPergunta\JogoPerguntaGourmetRepository;
use App\Services\Command\CommandService;
use App\Services\Command\iCommandService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(iJogoPerguntaRepository::class, function() {
            return new JogoPerguntaGourmetRepository;
        });

        $this->app->bind(iCommandService::class, function() {
            return new CommandService(new LeituraEntrada());
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
