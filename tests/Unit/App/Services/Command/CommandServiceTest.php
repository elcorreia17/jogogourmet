<?php

namespace Tests\Unit\App\Services\Command;

use App\Helpers\LeituraEntrada;
use App\Services\Command\CommandService;
use Tests\TestCase;

# sail php artisan test --filter=CommandServiceTest
class CommandServiceTest extends TestCase
{

    public function test_escrever_linha(){

        $input = "Pense em um prato que gosta (pressione enter após pensar)!";

        $mockReadLine = $this->getMockBuilder(LeituraEntrada::class)
           ->getMock();

        $mockReadLine->expects($this->once())
            ->method('ler')
            ->willReturn($input);

        $commandService = new CommandService($mockReadLine);
        $retorno = $commandService->escreverLinha("Pense em um prato que gosta (pressione enter após pensar)!");

        $this->assertEquals($retorno, $input, 'Texto difere do esperado.');
    }

    public function test_escrever_linha_com_resposta_fixa(){

        $input = "Escolha sim ou não?";
        $opcoes = ['sim', 'não'];
        $esperado = 'sim';

        $mockReadLine = $this->getMockBuilder(LeituraEntrada::class)
            ->getMock();

        $mockReadLine->expects($this->once())
            ->method('ler')
            ->willReturn($esperado);

        $commandService = new CommandService($mockReadLine);

        $retorno = $commandService->escreverLinhaComRespostaFixa($input, $opcoes);

        $this->assertEquals($retorno, $esperado, 'Texto difere do esperado.');
    }

}
