<?php

namespace Tests\Unit\App\Repositories\JogoPergunta;

use App\Repositories\JogoPergunta\JogoPerguntaGourmetRepository;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

# sail php artisan test --filter=JogoPerguntaGourmetRepositoryTest
class JogoPerguntaGourmetRepositoryTest extends TestCase
{

    public function test_criar_e_listar()
    {

        Redis::flushDB();

        $repository = new  JogoPerguntaGourmetRepository();

        $perguntaBasica = ['massa' => ["lasanha" => "lasanha",]];

        $repository->criar($perguntaBasica);

        $itensNoBancoDeDados = last($repository->listar());

        $this->assertTrue($perguntaBasica === $itensNoBancoDeDados, 'Registro não foi encontrado no Banco de Dados');
    }

    public function test_obter_nome_da_tabela()
    {

        $repository = new  JogoPerguntaGourmetRepository();

        $nomeDaTabela = $repository->obterNomeDaTabela();

        $this->assertIsString($nomeDaTabela, 'Nome da tabela deve ser string!');

        $this->assertTrue(strlen($nomeDaTabela) > 0, 'Nome da tabela não foi preencido!');
    }
}
