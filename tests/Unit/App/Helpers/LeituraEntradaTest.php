<?php

namespace Tests\Unit\App\Helpers;

use App\Helpers\LeituraEntrada;
use App\Services\Command\CommandService;
use Tests\TestCase;

# sail php artisan test --filter=LeituraEntradaTest
class LeituraEntradaTest extends TestCase
{
    public function test_ler(){

        $input = "Esse Deve ser o retorno do metodo";

        $mockReadLine = $this->getMockBuilder(LeituraEntrada::class)
            ->getMock();

        $mockReadLine->expects($this->once())
            ->method('ler')
            ->willReturn($input);

        $commandService = new CommandService($mockReadLine);

        $retorno = $commandService->escreverLinha("Esse Deve ser o retorno do metodo");

        $this->assertIsString($retorno);

        $this->assertEquals($retorno, $input, 'Texto difere do esperado.');    }
}
