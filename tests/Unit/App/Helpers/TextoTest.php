<?php

namespace Tests\Unit\App\Helpers;

use App\Helpers\Texto;
use Tests\TestCase;

# sail php artisan test --filter=TextoTest
class TextoTest extends TestCase
{
    public function test_remover_acentos(){

        $textoDeEntrada = "àáãéêíôç";

        $textoSaida = Texto::removerAcentos($textoDeEntrada);

        $this->assertIsString($textoSaida, "O retorno deveria ser uma string");

        $this->assertTrue($textoSaida === "aaaeeioc", 'O retorno é diferente do esperado');
    }

    public function test_transformar_em_minusculo(){

        $textoDeEntrada = "Lorem ipsum dolor sit amet, consectetur adipisci elit, ÇÁÉÚÃÊ. ";

        $textoSaida = Texto::transformarEmMinusculo($textoDeEntrada);

        $this->assertIsString($textoSaida, "O retorno deveria ser uma string");

        $this->assertTrue($textoSaida === "lorem ipsum dolor sit amet, consectetur adipisci elit, çáéúãê. ", 'O retorno é diferente do esperado');
    }

    public function test_remover_todos_os_espacos(){

        $textoDeEntrada = "Lorem ipsum dolor sit amet, consectetur adipisci elit, ÇÁÉÚÃÊ. ";

        $textoSaida = Texto::removerTodosOsEspacos($textoDeEntrada);

        $this->assertIsString($textoSaida, "O retorno deveria ser uma string");

        $this->assertTrue($textoSaida === "Loremipsumdolorsitamet,consecteturadipiscielit,ÇÁÉÚÃÊ.", 'O retorno é diferente do esperado');
    }
}
