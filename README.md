# Projeto jogo gourmet

Exemplo do Jogo Gourmet: <https://www.dropbox.com/s/bbek2kmd2dvvumh/JogoGourmet.zip?dl=0>

Você deverá implementar em PHP com exatamente o mesmo comportamento e características, o mais OO e o mais simples possível. Precisamos que nos envie o código fonte e o executável para auxiliar na avaliação da sua aplicação.
## Observações
Esse projeto foi desenvolvido em um ambiente windows usando docker + wsl 2.

<span style="color:#1589F0;"><b>Você precisa ter no mínimo o PHP 8.1 instalado para executar esse projeto!</b></span>

## Iniciar projeto

Clonar esse repositório em sua máquima local (se for windows, clonar dentro de sua versão linux do wsl, por questão de perfomance).

```
git clone https://gitlab.com/elcorreia17/jogogourmet.git
```

Dentro do diretório onde o projeto foi clonado realize os seguintes passos:

```bash
composer install
```

Em seguida você precisar fazer uma cópia do arquivo  `.env.example`  e renomear para `.env` dentro da raiz do projeto.

Execute o comando para iniciar os containers do Docker:
```bash
./vendor/bin/sail up -d
```

Pronto! o projeto está rodando.

## Testar o jogo

<span style="color:#1589F0;"><b>Para jogar, basta executar o seguinte comando:</b></span>

```bash
./vendor/bin/sail artisan app:jogo-gourmet
```

Para resetar os dados:

```
./vendor/bin/sail artisan app:jogo-gourmet resetar
```

Para realizar os testes de unidade:
```bash
./vendor/bin/sail php artisan test
```
